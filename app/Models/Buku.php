<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    //
    protected $table ='bukus';
    protected $fillable = array('id','judul','kode','deskripsi');
    protected $primaryKey = 'id';
}
